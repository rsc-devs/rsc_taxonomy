<?php


function rsc_taxonomy_find_term_in_tree($root, $tid) {
  foreach ($root as $branch_tid => $branch) {
    // Look for direct children.
    if ($branch_tid == $tid) {
      return $branch;
    }

    // Search within every child.
    $found = rsc_taxonomy_find_term_in_tree($branch->children, $tid);
    if ($found !== FALSE) {
      return $found;
    }
  }
  return FALSE;
}


/**
 * Displays all nodes associated with a term.
 *
 * TODO: cache the render array?
 */
function rsc_taxonomy_term_page($term) {

  // prevent menu from setting different title
  drupal_set_title($term->name);

  // Add RSS feed
  drupal_add_feed('taxonomy/term/' . $term->tid . '/feed', 'RSS - ' . $term->name);

  // Find our tid in the tree
  $subtree = rsc_taxonomy_find_term_in_tree(rsc_taxonomy_get_vocab_tree($term->vid), $term->tid);
  
  // Get the module settings
  $settings = variable_get('rsc_taxonomy_settings', null);

  // Determine whether this listing should be sorted:
  $sort_field = null;
  if (!empty($settings['sort_enable_field']) && !empty($settings['sort_weight_field'])) {
    
    // Get the value of the first item in this field instance for the current term
    $items = field_get_items('taxonomy_term', $term, $settings['sort_enable_field']);
    if (!empty($items)) {
      $item = array_shift($items);
      
      // If the field value evaluates to true, then sorting is enabled.
      if ($item['value']) {
        $sort_field = $settings['sort_weight_field'];
      }
    }
  }

  // Get a paginated list of node ids
  $n_nodes_per_page = isset($settings['nodes_per_page']) ? (int) $settings['nodes_per_page'] : 10;
  $nids = rsc_taxonomy_select_nodes($term->tid, true, $n_nodes_per_page, $sort_field);

  // Get the renderable taxonomy term
  $term_view_mode = 'full';
  $langcode = $GLOBALS['language_content']->language;
  // NB Always call this, even if no description, because other modules need hook_taxonomy_term_view_alter and hook_entity_view_alter
  $term_renderarray = taxonomy_term_view($term, $term_view_mode, $langcode);

  // start building the page
  $build = array();

  // Add term heading if the term has a description
  if (!empty($term->description)) {
    $build['term_heading'] = array(
      '#prefix' => '<div class="term-listing-heading">',
      '#suffix' => '</div>',
      'term' => $term_renderarray,
      '#weight' => 1
    );
  }

  // Show the sub-terms just like on the browse page if there are any.
  if ($subtree->children) {
    $build['subterms'] = [
      'list' => [
        '#theme' => 'item_list',
        '#prefix' => '<div class="browse-taxonomy browse-taxonomy-no-header">',
        '#suffix' => '</div>',
        '#items' => rsc_taxonomy_get_browse_items($subtree->children),
      ]
    ];
  }

  // Add a list of nodes if there are nodes
  if ($nids) {
    $build['nodes'] = array(
      '#prefix' => '<div class="taxonomy-nodes-wrapper">',
      '#suffix' => '</div>',
      'nodes' => node_view_multiple(node_load_multiple($nids)),
      '#weight' => 3
    );
  }

  // Add a message if there is nothing to see
  if (!($nids || $subtree)) {
    $build['nothing'] = array(
      '#markup' => '<strong>'.t('There is currently no content in this category.').'</strong>',
      '#weight' => 3
    );
  }

  // Put a pager at the top of the page
  $build['pager_top'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array('pager-top'),
    ),
    '#weight' => -5,
    'pager' => array(
      '#theme' => 'pager',
    ),
  );

  // Put a pager at the bottom of the page
  $build['pager_bottom'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array('pager-bottom'),
    ),
    '#weight' => 5,
    'pager' => array(
      '#theme' => 'pager',
    ),
  );

  // Keep modules that expect $entity->content to exist happy
  // otherwise this causes errors with implementation field_conditional_state_entity_view()
  $term->content = $build;

  // Allow modules to make their own additions to the term.
  module_invoke_all('taxonomy_term_view', $term, $term_view_mode, $langcode);
  module_invoke_all('entity_view', $term, 'taxonomy_term', $term_view_mode, $langcode);

  return $build;
}


/**
 * A fork of `taxonomy_select_nodes` from `taxonomy.module`.
 *
 * Allows us to sort on a field, not just on columns in the taxonomy index table.
 *
 * @param int $tid
 *   The term ID.
 * @param bool $pager
 *   Boolean to indicate whether a pager should be used.
 * @param bool $limit
 *   Integer. The maximum number of nodes to find.
 *   Set to FALSE for no limit.
 * @param string $sort_field
 *   The machine name of the field whose table to join and to sort by.
 * @param string $sort_field_column
 *   The name of the field property to sort by.
 *   For most fields, this is "value".
 *
 * @return array
 *   An array of node IDs matching the query.
 */
function rsc_taxonomy_select_nodes($tid, $pager = TRUE, $limit = FALSE, $sort_field = NULL, $sort_field_column = "value") {
  // If there is no taxonomy index, we can't query anything.
  if (!variable_get('taxonomy_maintain_index_table', TRUE)) {
    return [];
  }
  $query = db_select('taxonomy_index', 't');
  $query->addTag('node_access');

  // If a sort field was specified, join its table on sort on the "value" column
  // from that table.
  if ($sort_field) {
    $query->leftJoin("field_data_$sort_field", 'w', 't.nid = w.entity_id');
    $query->orderBy("w.{$sort_field}_{$sort_field_column}");
  }
  $query->condition('t.tid', $tid);
  if ($pager) {
    $count_query = clone $query;
    $count_query->addExpression('COUNT(t.nid)');

    $query = $query->extend('PagerDefault');
    if ($limit !== FALSE) {
      $query = $query->limit($limit);
    }
    $query->setCountQuery($count_query);
  }
  else {
    if ($limit !== FALSE) {
      $query->range(0, $limit);
    }
  }
  $query->addField('t', 'nid');
  $query->addField('t', 'tid');

  // After the specified field, sort by sticky first, then oldest first.
  $query->addField('t', 'sticky');
  $query->addField('t', 'created');
  $query->orderBy('t.sticky', 'DESC');
  $query->orderBy('t.created', 'DESC');

  return $query->execute()->fetchCol();
}


/**
 * @param int $vid
 *   The ID of the vocabulary.
 *
 * @return string
 *   The title of the browse page for this vocabulary.
 */
function rsc_taxonomy_browse_title($vid) {
  $vocab = taxonomy_vocabulary_load($vid);
  return t('Browse all @vocab', [
    '@vocab' => strtolower($vocab->name),
  ]);
}


function rsc_taxonomy_get_browse_items($tree) {
  $items = [];
  foreach ($tree as $tid => &$term) {
    $nodes = ($term->nodes) ? " ({$term->nodes})" : "";
    $items[$tid] = [
      'data' => l($term->name . $nodes, "taxonomy/term/{$term->tid}"),
      'children' => rsc_taxonomy_get_browse_items($term->children),
    ];
  }
  return $items;
}


/**
 * Page callback.
 * This function is temporary.
 *
 * TODO: Cache this in local storage on the user's browser, just like the menu.
 *
 * @param int $vid
 *   The ID of the vocabulary to generate the browse page for.
 *
 * @return array
 *   A renderable page array.
 */
function rsc_taxonomy_browse($vid) {
  // Try to get render array from cache first.
  $cid = "rsc_taxonomy_browse_$vid";
  $content = cache_get($cid, 'cache');

  if (empty($content)) {
    // Data is not available from cache. Build the render array.
    $content = [
      'list' => [
        '#theme' => 'item_list',
        '#prefix' => '<div class="browse-taxonomy browse-taxonomy-header">',
        '#suffix' => '</div>',
        '#items' => rsc_taxonomy_get_browse_items(rsc_taxonomy_get_vocab_tree($vid)),
      ]
    ];
    // Cache the render array permanently (can be rebuilt using the admin form).
    cache_set($cid, $content, 'cache', CACHE_PERMANENT);
  }
  else {
    // Data is available from cache.
    $content = $content->data;
  }

  return $content;
}
