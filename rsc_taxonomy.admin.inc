<?php


/**
 * A form builder function to be called by drupal_get_form to build the admin
 * page.
 *
 * @return array
 * A form array
 */
function rsc_taxonomy_admin_form() {
  $form = [];
  $variable_name = "rsc_taxonomy_settings";

  // Get a list of vocabularies keyed by vid
  $vocabs = taxonomy_get_vocabularies();
  $vocab_options = [];
  foreach ($vocabs as $vid => $vocab) {
    $vocab_options[$vid] = "{$vocab->name} (vid={$vid})";
  }

  // Get the current settings, and fill in the blanks with default settings.
  $settings = variable_get($variable_name, []) + [
      'menus_enabled' => [],
      'browse_pages_enabled' => [],
      'term_pages_enabled' => [],
      'sort_enable_field' => NULL,
      'sort_weight_field' => NULL,
      'nodes_per_page' => 10,
    ];

  // Get a list of enabled vocabularies
  $vocabs_enabled = [];
  foreach ($vocab_options as $key => $value) {
    if (
      !empty($settings['menus_enabled'][$key]) ||
      !empty($settings['browse_pages_enabled'][$key]) ||
      !empty($settings['term_pages_enabled'][$key])
    ) {
      $vocabs_enabled[$key] = $value;
    }
  }

  /*
   * Construct two lists:
   * - All fields that have instances on taxonomy terms
   * - All fields that have instances on nodes
   */
  $fieldmap = field_info_field_map();
  $taxonomy_fields = [NULL => "--none--",];
  $node_fields = [NULL => "--none--",];
  foreach ($fieldmap as $fieldname => $fieldinfo) {
    if (!empty($fieldinfo['bundles']['taxonomy_term'])) {
      $taxonomy_fields[$fieldname] = $fieldname;
    }
    if (!empty($fieldinfo['bundles']['node'])) {
      $node_fields[$fieldname] = $fieldname;
    }
  }

  // build the form

  $form['menus_enabled'] = [
    '#type' => 'checkboxes',
    '#required' => FALSE,
    '#multiple' => TRUE,
    '#title' => t('Enable vocabulary menus'),
    '#description' => t('Enable creating menu blocks for these vocabularies. This does not use the Drupal menu system, but our own. NOTE: Until access checks are implemented, this exposes all taxonomy terms in the selected vocabularies to the public.'),
    '#default_value' => $settings['menus_enabled'],
    '#options' => $vocab_options,
  ];

  $form['browse_pages_enabled'] = [
    '#type' => 'checkboxes',
    '#required' => FALSE,
    '#multiple' => TRUE,
    '#title' => t('Enable vocabulary browse pages'),
    '#description' => t('Enable the display of browse pages for these vocabularies. NOTE: Until access checks are implemented, this exposes all taxonomy terms in the selected vocabularies to the public.'),
    '#default_value' => $settings['browse_pages_enabled'],
    '#options' => $vocab_options,
  ];

  $form['term_pages_enabled'] = [
    '#type' => 'checkboxes',
    '#required' => FALSE,
    '#multiple' => TRUE,
    '#title' => t('Enable term pages'),
    '#description' => t('Enable the display of taxonomy term pages for these vocabularies.'),
    '#default_value' => $settings['term_pages_enabled'],
    '#options' => $vocab_options,
  ];

  $form['sort_enable_field'] = [
    '#title' => t('Field used to enable/disable sorting on taxonomy pages'),
    '#description' => t('The should be a field instance on taxonomy terms. The field value should evaluate to TRUE for taxonomy terms that should have sorted node listings.'),
    '#type' => 'select',
    '#multiple' => FALSE,
    '#required' => FALSE,
    '#options' => $taxonomy_fields,
    '#default_value' => $settings['sort_enable_field'],
  ];

  $form['sort_weight_field'] = [
    '#title' => t('Field used for node weight on sorted taxonomy pages'),
    '#description' => t('This should be a field instance on nodes. The field value is used to sort the nodes, if node sorting is enabled for the term being listed.'),
    '#type' => 'select',
    '#multiple' => FALSE,
    '#required' => FALSE,
    '#options' => $node_fields,
    '#default_value' => $settings['sort_weight_field'],
  ];

  $form['nodes_per_page'] = [
    '#title' => t('Number of nodes per page on taxonomy term pages'),
    '#description' => t('If there are more nodes than this, they will be paginated.'),
    '#type' => 'textfield',
    '#size' => 4,
    '#multiple' => FALSE,
    '#required' => TRUE,
    '#default_value' => $settings['nodes_per_page'],
    '#element_validate' => ['rsc_taxonomy_validate_npp'],
  ];

  $form['save'] = [
    '#type' => 'submit',
    '#value' => t('Save'),
  ];

  if (count($vocabs_enabled)) {

    $form['menus_rebuild'] = [
      '#type' => 'checkboxes',
      '#required' => FALSE,
      '#multiple' => TRUE,
      '#title' => t('Rebuild vocabulary menus and pages'),
      '#description' => t('Select the vocabularies to clear their caches and rebuild them.'),
      '#default_value' => [],
      '#options' => $vocabs_enabled,
    ];

    $form['rebuild'] = [
      '#type' => 'submit',
      '#value' => t('Rebuild selected menus'),
      '#submit' => ['rsc_taxonomy_admin_form_submit_rebuild'],
      '',
    ];

  }

  return $form;
}


function rsc_taxonomy_validate_npp($element, &$form_state, $form) {
  if (empty($element['#value']) || !is_numeric($element['#value']) || ((int) $element['#value']) != $element['#value'] || $element['#value'] < 1) {
    form_error($element, t('This must be an integer greater than 0.'));
  }
}


function rsc_taxonomy_admin_form_submit($form, &$form_state) {
  variable_set("rsc_taxonomy_settings", [
    'menus_enabled' => $form_state['values']['menus_enabled'],
    'browse_pages_enabled' => $form_state['values']['browse_pages_enabled'],
    'term_pages_enabled' => $form_state['values']['term_pages_enabled'],
    'sort_enable_field' => $form_state['values']['sort_enable_field'],
    'sort_weight_field' => $form_state['values']['sort_weight_field'],
    'nodes_per_page' => $form_state['values']['nodes_per_page'],
  ]);
}


function rsc_taxonomy_admin_form_submit_rebuild($form, &$form_state) {
  foreach ($form_state['values']['menus_rebuild'] as $vid => $must_rebuild) {
    if ($must_rebuild) {

      // clear caches
      cache_clear_all("rsc_taxonomy_vocab_tree_$vid", 'cache');
      cache_clear_all("rsc_taxonomy_menu_markup_$vid", 'cache');
      cache_clear_all("rsc_taxonomy_browse_$vid", 'cache');

      // regenerate markup
      rsc_taxonomy_get_menu_markup($vid);
      module_load_include('inc', 'rsc_taxonomy', 'rsc_taxonomy.pages');
      rsc_taxonomy_browse($vid);

    }
  }
}
